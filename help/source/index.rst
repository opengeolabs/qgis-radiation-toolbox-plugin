QGIS Radiation Toolbox Plugin Documentation
===========================================

This plugin allows to easily load data from `Safecast
<https://en.wikipedia.org/wiki/Safecast_(organization)>`__ radiation
monitoring devices (LOG file) like `bGeigie Nano <https://safecast.org/devices/>`__ or `CzechRad <https://github.com/juhele/CzechRad/>`__ into QGIS version 3 as a
new vector point layer. The data are loaded using predefined color
style.

The user can, if necessary, easily select and remove unwanted parts of
the measurement set and save the corrected set as a new LOG file for
uploading to `Safecast web map <http://safecast.org/tilemap/>`__.

Basic and experimental support for viewing `ERS 2.0 format <https://github.com/juhele/opengeodata/tree/master/ERS_-_European_Radiometric_and_Spectrometry_format>`__ (European Radiometric and Spectrometry format) and some types of `PicoEnvirotec PEI files <https://github.com/juhele/opengeodata/tree/master/Ground_radiation_monitoring_DEMO_data>`__ is also available.

.. toctree::
   :maxdepth: 2

   intro
   installation
   user-manual
   about
