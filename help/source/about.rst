About
-----

Acknowledgement
^^^^^^^^^^^^^^^

National Radiation Protection Institute (SURO), Prague, Czech
Republic (`SURO <http://www.suro.cz>`__).

Developed by `OpenGeoLabs <http://opengeolabs.cz>`__, Prague, Czech Republic.

License
^^^^^^^

Source code (https://gitlab.com/opengeolabs/radiation-toolbox/qgis-radiation-toolbox-plugin)
licensed under GNU GPL 2 and higher.

Report bugs or enhancements
^^^^^^^^^^^^^^^^^^^^^^^^^^^

You have found a bug in our software or you wish to implement a new
feature. Please fill out bug/enhancement issue in `our bug-tracker
<https://gitlab.com/opengeolabs/radiation-toolbox/qgis-radiation-toolbox-plugin/issues>`__.

Known issues
^^^^^^^^^^^^

In QGIS 3.14 and lower imported LOG data may be not ordered
appropriately. See
https://gitlab.com/opengeolabs/radiation-toolbox/qgis-radiation-toolbox-plugin/-/issues/33.
