Installation
============

Go to :menuselection:`Plugins --> Manage and Install Plugins`:

.. figure:: images/004_plugins_menu.png
   :width: 400px

   Plugins menu.

You should have **Radiation ToolBox Plugin** available in QGIS plugins selection
and you can install it:

.. figure:: images/install_005_install_plugin.png

   Plugins menu - install Radiation ToolBox plugin.
   
and the plugin icon appears in the QGIS toolbar:

.. figure:: images/install_006_RadiationToolbox_plugin_in_QGIS.png

   Radiation ToolBox in QGIS toolbar.

and now the plugin is ready to use.
