<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>LayerBase</name>
    <message>
        <location filename="../layer/__init__.py" line="69"/>
        <source>Loading data...</source>
        <translation>Завантаження даних...</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>Warning</source>
        <translation>Попередження</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>{} invalid measurement(s) skipped (see message log for details)</source>
        <translation>{} недійсні вимірювання пропущено (подробиці дивиться у журналі повідомлень)</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>Data loaded</source>
        <translation>Дані завантажено</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation>{} елементи завантажено (за {:.2f} сек).</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="160"/>
        <source>Unable to create SQLite datasource: {}</source>
        <translation>Не вдалося створити джерело даних SQLite: {}</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="297"/>
        <source>Style &apos;{}&apos; not found</source>
        <translation>Стиль &apos;{}&apos; не знайдено</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="325"/>
        <source>Undefined style</source>
        <translation>Невідомий стиль</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidget</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="198"/>
        <source>Load or select Safecast layer in order to display ader statistics.</source>
        <translation>Завантажте або виберіть шар Safecast, щоб відобразити статистику потужності амбіентного еквівалента дози.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="234"/>
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="252"/>
        <source>Load or select Safecast layer in order to display ader plot.</source>
        <translation>Завантажте або виберіть шар Safecast, щоб відобразити графік потужності амбіентного еквівалента дози.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="707"/>
        <source>Plot</source>
        <translation>Графік</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="333"/>
        <source>Load radiation data file</source>
        <translation>Завантажити файл радіаційних даних</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Critical</source>
        <translation>Критичний</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="384"/>
        <source>Unsupported file extension {}</source>
        <translation>Непідтримуване розширення файлу {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Failed to load input file &apos;{0}&apos;.

Details: {1}</source>
        <translation>Не вдалося завантажити вхідний файл &apos;{0}&apos;.

Деталі:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="456"/>
        <source>Failed to apply style: {0}</source>
        <translation>Не вдалося застосувати стиль: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="481"/>
        <source>Invalid Safecast layer</source>
        <translation>Недійсний шар Safecast</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>Save layer as new LOG file</source>
        <translation>Зберегти шар як новий LOG файл</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>LOG file (*.LOG)</source>
        <translation>Файл LOG (*.LOG)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>Overwrite?</source>
        <translation>Перезаписати?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>File {} already exists. Do you want to overwrite it?.</source>
        <translation>Файл {} вже існує. Ви хочете перезаписати його?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Failed to save LOG file: {0}</source>
        <translation>Не вдалося зберегти LOG файл: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Delete?</source>
        <translation>Видалити?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Do you want to delete {} selected features? This operation cannot be reverted.</source>
        <translation>Ви бажаєте видалити {} вибраних елементів? Цю операцію неможливо скасувати.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>Info</source>
        <translation>Інфо</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="591"/>
        <source>No features selected. Nothing to be deleled.</source>
        <translation>Елементи не вибрано. Нічого не потрібно видаляти.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="628"/>
        <source>No layer loaded or selected</source>
        <translation>Жоден шар не завантажений і не вибраний</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>No active layer available.</source>
        <translation>Немає активного шару.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="669"/>
        <source>Measured points</source>
        <translation>Виміряні точки</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="674"/>
        <source>Layer statistics - {}</source>
        <translation>Статистика шару - {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Route information</source>
        <translation>Інформація про маршрут</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average speed (km/h)</source>
        <translation>середня швидкість (км/год)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total monitoring time</source>
        <translation>загальний час моніторингу</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total distance (km)</source>
        <translation>загальна відстань (км)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Radiation values</source>
        <translation>Значення радіації</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>maximum dose rate (microSv/h)</source>
        <translation>максимальна потужність дози (мікроЗв/год)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average dose rate (microSv/h)</source>
        <translation>середня потужність дози (мікроЗв/год)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total dose (microSv)</source>
        <translation>загальна доза (мікроЗв)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="703"/>
        <source>Layer plot - {}</source>
        <translation>Графік шару - {}</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidgetBase</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="26"/>
        <source>Radiation Toolbox (DEV)</source>
        <translation>Радіаційний інструментарій (DEV)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="46"/>
        <source>Load and Edit</source>
        <translation>Завантажити та редагувати</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="282"/>
        <source>Style:</source>
        <translation>Стиль:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="80"/>
        <source>Apply</source>
        <translation>Застосувати</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="104"/>
        <source>Stats</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="134"/>
        <source>ADER statistics</source>
        <translation>ADER статистика</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="148"/>
        <source>Plot</source>
        <translation>Графік</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="170"/>
        <source>ADER plot</source>
        <translation>ADER графік</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="178"/>
        <source>Maps</source>
        <translation>Карти</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="200"/>
        <source>Online maps</source>
        <translation>Онлайн карти</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="212"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="228"/>
        <source>Map:</source>
        <translation>Карта:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="252"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="274"/>
        <source>Plot settings</source>
        <translation>Налаштування графіку</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="290"/>
        <source>Lines</source>
        <translation>Лінії</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="295"/>
        <source>Points</source>
        <translation>Точки</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="334"/>
        <source>Storage settings</source>
        <translation>Параметри зберігання</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="340"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="348"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="353"/>
        <source>GeoPackage</source>
        <translation>GeoPackage</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="358"/>
        <source>Memory</source>
        <translation>Пам'ять</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="379"/>
        <source>Import</source>
        <translation>Імпорт</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="382"/>
        <source>Import radiation data</source>
        <translation>Імпорт радіаційних даних</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="385"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="394"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="397"/>
        <source>Save layer into new file</source>
        <translation>Зберегти шар у новий файл</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="400"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="409"/>
        <source>Select</source>
        <translation>Обрати</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="412"/>
        <source>Select features to cut</source>
        <translation>Виберіть елементи для вирізання</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="421"/>
        <source>Deselect</source>
        <translation>Скасувати вибір</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="424"/>
        <source>Deselect features</source>
        <translation>Скасувати вибір елементів</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="433"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="436"/>
        <source>Delete selected features</source>
        <translation>Видалити обрані елементи</translation>
    </message>
</context>
<context>
    <name>SafecastLayer</name>
    <message>
        <location filename="../safecast_layer.py" line="86"/>
        <source>ADER microSv/h</source>
        <translation type="obsolete">ADER мікроЗв/год</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="87"/>
        <source>Local time</source>
        <translation type="obsolete">Місцевий час</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="88"/>
        <source>Device</source>
        <translation type="obsolete">Пристрій</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="89"/>
        <source>Device ID</source>
        <translation type="obsolete">Пристрій ID</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="90"/>
        <source>Datetime</source>
        <translation type="obsolete">Дата та час</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="91"/>
        <source>CPM</source>
        <translation type="obsolete">CPM</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="92"/>
        <source>Pulses 5sec</source>
        <translation type="obsolete">Імпульси 5 сек</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="93"/>
        <source>Pulses total</source>
        <translation type="obsolete">Всього імпульсів</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="94"/>
        <source>Validity</source>
        <translation type="obsolete">Дійсність</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="95"/>
        <source>Latitude (deg)</source>
        <translation type="obsolete">Широта (град)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="96"/>
        <source>Hemisphere</source>
        <translation type="obsolete">Півкуля</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="97"/>
        <source>Longitude (deg)</source>
        <translation type="obsolete">Довгота (град)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="98"/>
        <source>East/West</source>
        <translation type="obsolete">Захід/Схід</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="99"/>
        <source>Altitude</source>
        <translation type="obsolete">Висота над рівнем моря</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="100"/>
        <source>GPS Validity</source>
        <translation type="obsolete">Дійсність GPS</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="101"/>
        <source>Sat</source>
        <translation type="obsolete">Sat</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="102"/>
        <source>HDOP</source>
        <translation type="obsolete">HDOP</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="103"/>
        <source>CheckSum</source>
        <translation type="obsolete">CheckSum</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="138"/>
        <source>Loading data...</source>
        <translation type="obsolete">Завантаження даних...</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>Info</source>
        <translation type="obsolete">Інфо</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation type="obsolete">{} елементи завантажено (за {:.2f} сек).</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="119"/>
        <source>Failed to read input data. Line: {}</source>
        <translation>Не вдалося прочитати вхідні дані. Рядочок: {}</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="222"/>
        <source>unknown</source>
        <translation type="obsolete">невідомий</translation>
    </message>
</context>
<context>
    <name>SafecastLayerHelper</name>
    <message>
        <location filename="../layer/safecast.py" line="282"/>
        <source>Unable to retrive Safecast metadata for selected layer</source>
        <translation>Не вдалося отримати метадані Safecast для вибраного шару</translation>
    </message>
</context>
<context>
    <name>SafecastPlot</name>
    <message>
        <location filename="../tools/plot/safecast.py" line="85"/>
        <source>Distance (km)</source>
        <translation>Відстань (км)</translation>
    </message>
    <message>
        <location filename="../tools/plot/safecast.py" line="90"/>
        <source>ADER (microSv/h)</source>
        <translation>ADER (мікроЗв/год)</translation>
    </message>
</context>
<context>
    <name>self._layer</name>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>Warning</source>
        <translation>Попередження</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>No valid date found. Unable to fix datetime.</source>
        <translation>Дійсна дата не знайдена. Не вдалося виправити дату й час.</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="537"/>
        <source>unknown</source>
        <translation>невідомий</translation>
    </message>
</context>
</TS>
