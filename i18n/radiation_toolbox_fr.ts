<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>LayerBase</name>
    <message>
        <location filename="../layer/__init__.py" line="69"/>
        <source>Chargement de données...</source>
        <translation>Caricamento dati...</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>Attention</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>{} invalid measurement(s) skipped (see message log for details)</source>
        <translation>{} mesure non valable (lire les détails dans le journal des messages)</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>Data loaded</source>
        <translation>Données chargées</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation>{} entités chargées (en {: .2f} sec).</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="160"/>
        <source>Unable to create SQLite datasource: {}</source>
        <translation>Création de source de données SQLite échouée: {}</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="297"/>
        <source>Style &apos;{}&apos; not found</source>
        <translation>Style &quot;{}&quot; pas trouvé</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="325"/>
        <source>Undefined style</source>
        <translation>Style non défini</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidget</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="198"/>
        <source>Load or select Safecast layer in order to display ader statistics.</source>
        <translation>Chargez ou sélectionnez la couche Safecast pour afficher les statistiques ader.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="234"/>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="252"/>
        <source>Load or select Safecast layer in order to display ader plot.</source>
        <translation>Chargez ou sélectionnez la couche Safecast pour afficher le tracé des données ader.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="707"/>
        <source>Plot</source>
        <translation>Tracé</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="333"/>
        <source>Load radiation data file</source>
        <translation>Charger le fichier des données de rayonnement</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Critical</source>
        <translation>Critique</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="384"/>
        <source>Unsupported file extension {}</source>
        <translation>Extension de fichier non pris en charge {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Failed to load input file &apos;{0}&apos;.

Details: {1}</source>
        <translation>Chargement de fichier d'entrée échoué &apos;{0}&apos;.

Dettagli: {1}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="456"/>
        <source>Failed to apply style: {0}</source>
        <translation>Application de style échoué: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="481"/>
        <source>Invalid Safecast layer</source>
        <translation>Couche Safecast non valable</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>Save layer as new LOG file</source>
        <translation>Enregistrer la couche sous un nouveau fichier LOG</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>LOG file (*.LOG)</source>
        <translation>Fichier LOG (* .LOG)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>Overwrite?</source>
        <translation>Écraser ?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>File {} already exists. Do you want to overwrite it?.</source>
        <translation>Le fichier {} existe déja. Voulez-vous l'écraser ?.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Failed to save LOG file: {0}</source>
        <translation>Enregistrement du fichier LOG échoué: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Delete?</source>
        <translation>Supprimer ?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Do you want to delete {} selected features? This operation cannot be reverted.</source>
        <translation>Voulez-vouse supprimer les {} entités sélectionnées ? Cette opération ne peut pas être annulée.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>Info</source>
        <translation>Informations</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="591"/>
        <source>No features selected. Nothing to be deleled.</source>
        <translation>Aucune entité sélectionnée. Rien à supprimer.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="628"/>
        <source>No layer loaded or selected</source>
        <translation>Aucune couche chargée ou sélectionnée</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>No active layer available.</source>
        <translation>Aucune couche active sélectionnée.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="669"/>
        <source>Measured points</source>
        <translation>Points mesurés</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="674"/>
        <source>Layer statistics - {}</source>
        <translation>Statistiques sur la couche - {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Route information</source>
        <translation>Information sur le trajet</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average speed (km/h)</source>
        <translation>Vitesse moyenne (km / h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total monitoring time</source>
        <translation>temps total de surveillance</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total distance (km)</source>
        <translation>distance totale (km)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Radiation values</source>
        <translation>Valeurs de rayonnement</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>maximum dose rate (microSv/h)</source>
        <translation>débit de dose maximal  (microSv / h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average dose rate (microSv/h)</source>
        <translation>débit de dose moyen (microSv / h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total dose (microSv)</source>
        <translation>dose totale (microSv)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="703"/>
        <source>Layer plot - {}</source>
        <translation>Tracé de couche - {}</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidgetBase</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="26"/>
        <source>Radiation Toolbox (DEV)</source>
        <translation>Toolbox rayonnement (DEV)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="46"/>
        <source>Load and Edit</source>
        <translation>Charger et Éditer</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="282"/>
        <source>Style:</source>
        <translation>Style:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="80"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="104"/>
        <source>Stats</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="134"/>
        <source>ADER statistics</source>
        <translation>Statistiques ADER</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="148"/>
        <source>Plot</source>
        <translation>Tracé</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="170"/>
        <source>ADER plot</source>
        <translation>Tracé ADER</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="178"/>
        <source>Maps</source>
        <translation>Cartes</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="200"/>
        <source>Online maps</source>
        <translation>Cartes en line</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="212"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="228"/>
        <source>Map:</source>
        <translation>Carte:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="252"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="274"/>
        <source>Plot settings</source>
        <translation>Réglages de tracé</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="290"/>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="295"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="334"/>
        <source>Storage settings</source>
        <translation>Réglages de reangement</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="340"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="348"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="353"/>
        <source>GeoPackage</source>
        <translation>GeoPackage</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="358"/>
        <source>Memory</source>
        <translation>Mémoire</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="379"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="382"/>
        <source>Import radiation data</source>
        <translation>Importer données de rayonnement</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="385"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="394"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="397"/>
        <source>Save layer into new file</source>
        <translation>Enregistrer la couche dans un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="400"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="409"/>
        <source>Select</source>
        <translation>Selectionner</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="412"/>
        <source>Select features to cut</source>
        <translation>Sélectionner les entités à couper</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="421"/>
        <source>Deselect</source>
        <translation>Désélectionner</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="424"/>
        <source>Deselect features</source>
        <translation>Désélectionner les entités</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="433"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="436"/>
        <source>Delete selected features</source>
        <translation>Supprimer les entités sélectionnées</translation>
    </message>
</context>
<context>
    <name>SafecastLayer</name>
    <message>
        <location filename="../safecast_layer.py" line="86"/>
        <source>ADER microSv/h</source>
        <translation type="obsolete">ADER microSv/h</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="87"/>
        <source>Local time</source>
        <translation type="obsolete">Heure locale</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="88"/>
        <source>Device</source>
        <translation type="obsolete">Dispositif</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="89"/>
        <source>Device ID</source>
        <translation type="obsolete">ID du dispositif</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="90"/>
        <source>Datetime</source>
        <translation type="obsolete">Date et heure</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="91"/>
        <source>CPM</source>
        <translation type="obsolete">CPM</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="92"/>
        <source>Pulses 5sec</source>
        <translation type="obsolete">Impulsions 5 sec</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="93"/>
        <source>Pulses total</source>
        <translation type="obsolete">Total des impulsions</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="94"/>
        <source>Validity</source>
        <translation type="obsolete">Validité</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="95"/>
        <source>Latitude (deg)</source>
        <translation type="obsolete">Latitude (degrés)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="96"/>
        <source>Hemisphere</source>
        <translation type="obsolete">Hémisphere</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="97"/>
        <source>Longitude (deg)</source>
        <translation type="obsolete">Longitude (degrés)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="98"/>
        <source>East/West</source>
        <translation type="obsolete">Est / Ouest</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="99"/>
        <source>Altitude</source>
        <translation type="obsolete">Altitude</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="100"/>
        <source>GPS Validity</source>
        <translation type="obsolete">Validité GPS</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="101"/>
        <source>Sat</source>
        <translation type="obsolete">Sat</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="102"/>
        <source>HDOP</source>
        <translation type="obsolete">HDOP</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="103"/>
        <source>CheckSum</source>
        <translation type="obsolete">CheckSum</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="138"/>
        <source>Loading data...</source>
        <translation type="obsolete">Chargement de données...</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>Info</source>
        <translation type="obsolete">Informations</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation type="obsolete">{} entités chargées (en {: .2f} sec).</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="119"/>
        <source>Failed to read input data. Line: {}</source>
        <translation>Lecture de données d'entrée échouée. Ligne: {}</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="222"/>
        <source>unknown</source>
        <translation type="obsolete">inconnu(e)</translation>
    </message>
</context>
<context>
    <name>SafecastLayerHelper</name>
    <message>
        <location filename="../layer/safecast.py" line="282"/>
        <source>Unable to retrive Safecast metadata for selected layer</source>
        <translation>Récuperation des metadonnées Safecast pour la couche sélectionnée échouée</translation>
    </message>
</context>
<context>
    <name>SafecastPlot</name>
    <message>
        <location filename="../safecast_plot.py" line="85"/>
        <source>Distance (km)</source>
        <translation>Distance (km)</translation>
    </message>
    <message>
        <location filename="../safecast_plot.py" line="90"/>
        <source>ADER (microSv/h)</source>
        <translation>ADER (microSv/h)</translation>
    </message>
</context>
<context>
    <name>self._layer</name>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>No valid date found. Unable to fix datetime.</source>
        <translation>Aucune donnée valide trouvée. Correction de date-heure échouée.</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="537"/>
        <source>unknown</source>
        <translation>inconnu(e)</translation>
    </message>
</context>
</TS>
