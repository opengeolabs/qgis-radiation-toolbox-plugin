<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>LayerBase</name>
    <message>
        <location filename="../layer/__init__.py" line="69"/>
        <source>Loading data...</source>
        <translation>Ładowanie danych...</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>{} invalid measurement(s) skipped (see message log for details)</source>
        <translation>{} Pominięto nieprawidłowe pomiary (w celu szczegółowych informacji sprawdź wiadomość w pliku log)</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>Data loaded</source>
        <translation>Załadowano dane</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation>Załadowano punky (w ciągu {:.2f} sec).</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="160"/>
        <source>Unable to create SQLite datasource: {}</source>
        <translation>Nie można utworzyć źródła danych SQLite: {}</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="297"/>
        <source>Style &apos;{}&apos; not found</source>
        <translation>Styl &apos;{}&apos; nie znaleziony</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="325"/>
        <source>Undefined style</source>
        <translation>Nieznany styl</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidget</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="198"/>
        <source>Load or select Safecast layer in order to display ader statistics.</source>
        <translation>Załaduj lub wybierz warstwę Safecast, aby wyświetlić statystyki.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="234"/>
        <source>Statistics</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="252"/>
        <source>Load or select Safecast layer in order to display ader plot.</source>
        <translation>Załaduj lub wybierz warstwę Safecast, aby wyświetlić wykres ADER.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="707"/>
        <source>Plot</source>
        <translation>Wykres</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="333"/>
        <source>Load radiation data file</source>
        <translation>Załaduj plik z danymi promieniowania</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Critical</source>
        <translation>Krytyczny</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="384"/>
        <source>Unsupported file extension {}</source>
        <translation>Nieobsługiwane rozszerzenie pliku {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Failed to load input file &apos;{0}&apos;.

Details: {1}</source>
        <translation>Nie udało się załadować pliku wejściowego &apos;{0}&apos;.

Szczegóły: {1}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="456"/>
        <source>Failed to apply style: {0}</source>
        <translation>Nie udało się zastosować stylu: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="481"/>
        <source>Invalid Safecast layer</source>
        <translation>Nieprawidłowa warstwa Safecast</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>Save layer as new LOG file</source>
        <translation>Zapisz warstwę jako nowy plik LOG</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>LOG file (*.LOG)</source>
        <translation>Plik LOG (*.LOG)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>Overwrite?</source>
        <translation>Nadpisać?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>File {} already exists. Do you want to overwrite it?.</source>
        <translation>Plik {} już istnieje. Chcesz go nadpisać?.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Failed to save LOG file: {0}</source>
        <translation>Nie udało się zapisać pliku LOG: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Delete?</source>
        <translation>Usunąć?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Do you want to delete {} selected features? This operation cannot be reverted.</source>
        <translation>Czy chcesz usunąć wybrane elementy? Ta operacja jest nieodwracalna.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>Info</source>
        <translation>Informacje</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="591"/>
        <source>No features selected. Nothing to be deleled.</source>
        <translation>Nie wybrano elementów do usunięcia. Nic nie zostanie usunięte.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="628"/>
        <source>No layer loaded or selected</source>
        <translation>Żadna warstwa nie została załadowana lub wybrana</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>No active layer available.</source>
        <translation>Żadna warstwa aktywna nie jest dostępna.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="669"/>
        <source>Measured points</source>
        <translation>Punkty pomiarowe</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="674"/>
        <source>Layer statistics - {}</source>
        <translation>Statystyka warstw - {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Route information</source>
        <translation>Informacje o trasie</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average speed (km/h)</source>
        <translation>Średnia prędkość (km/h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total monitoring time</source>
        <translation>Całkowity czas pomiaru</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total distance (km)</source>
        <translation>Całkowita odległość (km)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Radiation values</source>
        <translation>Wartość promieniowania</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>maximum dose rate (microSv/h)</source>
        <translation>Maksymalna moc dawki (microSv/h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average dose rate (microSv/h)</source>
        <translation>Średnia moc dawki (microSv/h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total dose (microSv)</source>
        <translation>Całkowita dawka (microSv)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="703"/>
        <source>Layer plot - {}</source>
        <translation>Wykres warstwowy - {}</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidgetBase</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="26"/>
        <source>Radiation Toolbox (DEV)</source>
        <translation>Radiation Toolbox (DEV)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="46"/>
        <source>Load and Edit</source>
        <translation>Załaduj i edytuj</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="282"/>
        <source>Style:</source>
        <translation>Styl:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="80"/>
        <source>Apply</source>
        <translation>Użyj</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="104"/>
        <source>Stats</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="134"/>
        <source>ADER statistics</source>
        <translation>Statystyki ADER</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="148"/>
        <source>Plot</source>
        <translation>Wykres</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="170"/>
        <source>ADER plot</source>
        <translation>Wykres ADER</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="178"/>
        <source>Maps</source>
        <translation>Mapy</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="200"/>
        <source>Online maps</source>
        <translation>Mapy online</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="212"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="228"/>
        <source>Map:</source>
        <translation>Mapa:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="252"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="274"/>
        <source>Plot settings</source>
        <translation>Ustawienia wykresu</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="290"/>
        <source>Lines</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="295"/>
        <source>Points</source>
        <translation>Punkty</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="334"/>
        <source>Storage settings</source>
        <translation>Ustawienia przechowywania</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="340"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="348"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="353"/>
        <source>GeoPackage</source>
        <translation>GeoPackage</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="358"/>
        <source>Memory</source>
        <translation>Pamięć</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="379"/>
        <source>Import</source>
        <translation>Importuj</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="382"/>
        <source>Import radiation data</source>
        <translation>Załaduj dane promieniowania</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="385"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="394"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="397"/>
        <source>Save layer into new file</source>
        <translation>Zapisz warstwę w nowym pliku</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="400"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="409"/>
        <source>Select</source>
        <translation>Wybierz</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="412"/>
        <source>Select features to cut</source>
        <translation>Wybierz elementy do usunięcia</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="421"/>
        <source>Deselect</source>
        <translation>Odznacz</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="424"/>
        <source>Deselect features</source>
        <translation>Odznacz wybrane elementy</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="433"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="436"/>
        <source>Delete selected features</source>
        <translation>Usuń wybrane elementy</translation>
    </message>
</context>
<context>
    <name>SafecastLayer</name>
    <message>
        <location filename="../safecast_layer.py" line="86"/>
        <source>ADER microSv/h</source>
        <translation type="obsolete">ADER microSv/h</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="87"/>
        <source>Local time</source>
        <translation type="obsolete">Czas lokalny</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="88"/>
        <source>Device</source>
        <translation type="obsolete">Urządzenie</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="89"/>
        <source>Device ID</source>
        <translation type="obsolete">ID urządzenia</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="90"/>
        <source>Datetime</source>
        <translation type="obsolete">Data i godzina</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="91"/>
        <source>CPM</source>
        <translation type="obsolete">CPM</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="92"/>
        <source>Pulses 5sec</source>
        <translation type="obsolete">Impulsy w czasie 5s</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="93"/>
        <source>Pulses total</source>
        <translation type="obsolete">Całkowice impulsy</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="94"/>
        <source>Validity</source>
        <translation type="obsolete">Ważność</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="95"/>
        <source>Latitude (deg)</source>
        <translation type="obsolete">Zem. šířka (°)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="96"/>
        <source>Hemisphere</source>
        <translation type="obsolete">Polokoule</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="97"/>
        <source>Longitude (deg)</source>
        <translation type="obsolete">Zem. délka (°)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="98"/>
        <source>East/West</source>
        <translation type="obsolete">Východ/Západ</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="99"/>
        <source>Altitude</source>
        <translation type="obsolete">Nadm. výška</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="100"/>
        <source>GPS Validity</source>
        <translation type="obsolete">Platnost GPS</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="101"/>
        <source>Sat</source>
        <translation type="obsolete">Sat</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="102"/>
        <source>HDOP</source>
        <translation type="obsolete">HDOP</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="103"/>
        <source>CheckSum</source>
        <translation type="obsolete">CheckSum</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="138"/>
        <source>Loading data...</source>
        <translation type="obsolete">Načítám data...</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation type="obsolete">{} bodů načteno (během {:.2f} sec).</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="119"/>
        <source>Failed to read input data. Line: {}</source>
        <translation>Błąd odczytu wprowadzonych danych. Linia: {}</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="222"/>
        <source>unknown</source>
        <translation type="obsolete">neznámý</translation>
    </message>
</context>
<context>
    <name>SafecastLayerHelper</name>
    <message>
        <location filename="../layer/safecast.py" line="282"/>
        <source>Unable to retrive Safecast metadata for selected layer</source>
        <translation>Nie można pobrać metadanych Safecast dla wybranej warstwy</translation>
    </message>
</context>
<context>
    <name>SafecastPlot</name>
    <message>
        <location filename="../tools/plot/safecast.py" line="85"/>
        <source>Distance (km)</source>
        <translation>Odległość (km)</translation>
    </message>
    <message>
        <location filename="../tools/plot/safecast.py" line="90"/>
        <source>ADER (microSv/h)</source>
        <translation>ADER (microSv/h)</translation>
    </message>
</context>
<context>
    <name>self._layer</name>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>No valid date found. Unable to fix datetime.</source>
        <translation>Nie znaleziono poprawnej daty. Nie można ustalić daty i godziny.</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="537"/>
        <source>unknown</source>
        <translation>nieznany</translation>
    </message>
</context>
</TS>
