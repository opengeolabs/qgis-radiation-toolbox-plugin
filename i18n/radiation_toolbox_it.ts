<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>LayerBase</name>
    <message>
        <location filename="../layer/__init__.py" line="69"/>
        <source>Loading data...</source>
        <translation>Caricamento dati...</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>Warning</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="118"/>
        <source>{} invalid measurement(s) skipped (see message log for details)</source>
        <translation>{} misure non valide (vedi il registro messaggi per i dettagli)</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>Data loaded</source>
        <translation>Dati caricati</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="136"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation>{} funzionalità caricate (in {: .2f} sec).</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="160"/>
        <source>Unable to create SQLite datasource: {}</source>
        <translation>Impossibile creare la fonte dei dati SQLite: {}</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="297"/>
        <source>Style &apos;{}&apos; not found</source>
        <translation>Stile &quot;{}&quot; non trovato</translation>
    </message>
    <message>
        <location filename="../layer/__init__.py" line="325"/>
        <source>Undefined style</source>
        <translation>Stile indefinito</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidget</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="198"/>
        <source>Load or select Safecast layer in order to display ader statistics.</source>
        <translation>Caricare o selezionare il livello Safecast per visualizzare le statistiche ader.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="234"/>
        <source>Statistics</source>
        <translation>Statistica</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="252"/>
        <source>Load or select Safecast layer in order to display ader plot.</source>
        <translation>Caricare o selezionare il livello Safecast per visualizzare il grafico ader.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="707"/>
        <source>Plot</source>
        <translation>Disegna</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="333"/>
        <source>Load radiation data file</source>
        <translation>Carica il file con i dati di radiazioni</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Critical</source>
        <translation>Critico</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="384"/>
        <source>Unsupported file extension {}</source>
        <translation>Estensione di file non supportata {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="402"/>
        <source>Failed to load input file &apos;{0}&apos;.

Details: {1}</source>
        <translation>Impossibile caricare il file di input &apos;{0}&apos;.

Dettagli: {1}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="456"/>
        <source>Failed to apply style: {0}</source>
        <translation>Impossibile applicare lo stile: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="481"/>
        <source>Invalid Safecast layer</source>
        <translation>Livello Safecast non valido</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>Save layer as new LOG file</source>
        <translation>Salva livello come nuovo file LOG</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="488"/>
        <source>LOG file (*.LOG)</source>
        <translation>File LOG (* .LOG)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>Overwrite?</source>
        <translation>Sovrascrivere?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="507"/>
        <source>File {} already exists. Do you want to overwrite it?.</source>
        <translation>Il file {} esiste già. Vuoi sovrascriverlo?.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="521"/>
        <source>Failed to save LOG file: {0}</source>
        <translation>Impossibile salvare il file LOG: {0}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Delete?</source>
        <translation>Elimina?</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="566"/>
        <source>Do you want to delete {} selected features? This operation cannot be reverted.</source>
        <translation>Vuoi eliminare {} funzionalità selezionate? Questa operazione non può essere ripristinata.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>Info</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="591"/>
        <source>No features selected. Nothing to be deleled.</source>
        <translation>Nessuna funzionalità selezionata. Nulla da cancellare.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="628"/>
        <source>No layer loaded or selected</source>
        <translation>Nessun livello caricato o selezionato</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="630"/>
        <source>No active layer available.</source>
        <translation>Nessun livello attivo selezionato.</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="669"/>
        <source>Measured points</source>
        <translation>Punti misurati</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="674"/>
        <source>Layer statistics - {}</source>
        <translation>Statistiche sui livelli - {}</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Route information</source>
        <translation>Informazioni sul percorso</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average speed (km/h)</source>
        <translation>velocità media (km / h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total monitoring time</source>
        <translation>tempo di monitoraggio totale</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total distance (km)</source>
        <translation>distanza totale (km)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>Radiation values</source>
        <translation>Valori di radiazione</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>maximum dose rate (microSv/h)</source>
        <translation>dose massima (microSv / h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>average dose rate (microSv/h)</source>
        <translation>dose media (microSv / h)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="675"/>
        <source>total dose (microSv)</source>
        <translation>dose totale (microSv)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget.py" line="703"/>
        <source>Layer plot - {}</source>
        <translation>Grafico del livello - {}</translation>
    </message>
</context>
<context>
    <name>RadiationToolboxDockWidgetBase</name>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="26"/>
        <source>Radiation Toolbox (DEV)</source>
        <translation>Toolbox radiazioni (DEV)</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="46"/>
        <source>Load and Edit</source>
        <translation>Carica e modifica</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="282"/>
        <source>Style:</source>
        <translation>Stile:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="80"/>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="104"/>
        <source>Stats</source>
        <translation>Statistiche</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="134"/>
        <source>ADER statistics</source>
        <translation>Statistiche ADER</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="148"/>
        <source>Plot</source>
        <translation>Grafico</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="170"/>
        <source>ADER plot</source>
        <translation>Grafico ADER</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="178"/>
        <source>Maps</source>
        <translation>Mappe</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="200"/>
        <source>Online maps</source>
        <translation>Mappe online</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="212"/>
        <source>Add</source>
        <translation>Inserisci</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="228"/>
        <source>Map:</source>
        <translation>Mappa:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="252"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="274"/>
        <source>Plot settings</source>
        <translation>Impostazioni del grafico</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="290"/>
        <source>Lines</source>
        <translation>Linee</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="295"/>
        <source>Points</source>
        <translation>Punti</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="334"/>
        <source>Storage settings</source>
        <translation>Impostazioni di archiviazione</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="340"/>
        <source>Format:</source>
        <translation>Formato:</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="348"/>
        <source>SQLite</source>
        <translation>SQLite</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="353"/>
        <source>GeoPackage</source>
        <translation>GeoPackage</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="358"/>
        <source>Memory</source>
        <translation>Memoria</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="379"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="382"/>
        <source>Import radiation data</source>
        <translation>Importa dati sulle radiazioni</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="385"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="394"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="397"/>
        <source>Save layer into new file</source>
        <translation>Salva il livello in un nuovo file</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="400"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="409"/>
        <source>Select</source>
        <translation>Seleziona</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="412"/>
        <source>Select features to cut</source>
        <translation>Seleziona le caratteristiche da tagliare</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="421"/>
        <source>Deselect</source>
        <translation>Deseleziona</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="424"/>
        <source>Deselect features</source>
        <translation>Deseleziona le funzionalità</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="433"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../radiation_toolbox_dockwidget_base.ui" line="436"/>
        <source>Delete selected features</source>
        <translation>Elimina le funzionalità selezionate</translation>
    </message>
</context>
<context>
    <name>SafecastLayer</name>
    <message>
        <location filename="../safecast_layer.py" line="86"/>
        <source>ADER microSv/h</source>
        <translation type="obsolete">ADER microSv/h</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="87"/>
        <source>Local time</source>
        <translation type="obsolete">Ora locale</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="88"/>
        <source>Device</source>
        <translation type="obsolete">Dispositivo</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="89"/>
        <source>Device ID</source>
        <translation type="obsolete">ID del dispositivo</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="90"/>
        <source>Datetime</source>
        <translation type="obsolete">Data e ora</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="91"/>
        <source>CPM</source>
        <translation type="obsolete">CPM</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="92"/>
        <source>Pulses 5sec</source>
        <translation type="obsolete">Impulsi 5 sec</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="93"/>
        <source>Pulses total</source>
        <translation type="obsolete">Totale impulsi</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="94"/>
        <source>Validity</source>
        <translation type="obsolete">Validità</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="95"/>
        <source>Latitude (deg)</source>
        <translation type="obsolete">Latitudine (gradi)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="96"/>
        <source>Hemisphere</source>
        <translation type="obsolete">Emisfero</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="97"/>
        <source>Longitude (deg)</source>
        <translation type="obsolete">Longitudine (gradi)</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="98"/>
        <source>East/West</source>
        <translation type="obsolete">Est / Ovest</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="99"/>
        <source>Altitude</source>
        <translation type="obsolete">Altitudine</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="100"/>
        <source>GPS Validity</source>
        <translation type="obsolete">Validità GPS</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="101"/>
        <source>Sat</source>
        <translation type="obsolete">Sat</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="102"/>
        <source>HDOP</source>
        <translation type="obsolete">HDOP</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="103"/>
        <source>CheckSum</source>
        <translation type="obsolete">CheckSum</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="138"/>
        <source>Loading data...</source>
        <translation type="obsolete">Caricamento dati...</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>Info</source>
        <translation type="obsolete">Informazioni</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="159"/>
        <source>{} features loaded (in {:.2f} sec).</source>
        <translation type="obsolete">{} funzionalità caricate (in {: .2f} sec).</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="119"/>
        <source>Failed to read input data. Line: {}</source>
        <translation>Impossibile leggere i dati di input. Riga: {}</translation>
    </message>
    <message>
        <location filename="../safecast_layer.py" line="222"/>
        <source>unknown</source>
        <translation type="obsolete">sconosciuto</translation>
    </message>
</context>
<context>
    <name>SafecastLayerHelper</name>
    <message>
        <location filename="../layer/safecast.py" line="282"/>
        <source>Unable to retrive Safecast metadata for selected layer</source>
        <translation>Impossibile recuperare i metadati di Safecast per il livello selezionato</translation>
    </message>
</context>
<context>
    <name>SafecastPlot</name>
    <message>
        <location filename="../safecast_plot.py" line="85"/>
        <source>Distance (km)</source>
        <translation>Distanza (km)</translation>
    </message>
    <message>
        <location filename="../safecast_plot.py" line="90"/>
        <source>ADER (microSv/h)</source>
        <translation>ADER (microSv/h)</translation>
    </message>
</context>
<context>
    <name>self._layer</name>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>Warning</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="502"/>
        <source>No valid date found. Unable to fix datetime.</source>
        <translation>Nessuna data valida trovata. Impossibile correggere il datetime.</translation>
    </message>
    <message>
        <location filename="../layer/safecast.py" line="537"/>
        <source>unknown</source>
        <translation>sconosciuto</translation>
    </message>
</context>
</TS>
